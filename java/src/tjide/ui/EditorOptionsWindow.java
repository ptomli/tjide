/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package tjide.ui;

import java.util.ResourceBundle;

import gjexer.TAction;
import gjexer.TApplication;
import gjexer.TButton;
import gjexer.TCheckBox;
import gjexer.TField;
import gjexer.TWindow;
import gjexer.bits.CellAttributes;

/**
 * This window is used to configure the editor preferences.
 */
public class EditorOptionsWindow extends TWindow {

    /**
     * Translated strings.
     */
    private static ResourceBundle i18n = ResourceBundle.getBundle(EditorOptionsWindow.class.getName());

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The number of "undo/redo" levels.
     */
    private TField undoLevel;

    /**
     * The number of columns to indent.
     */
    private TField indentLevel;

    /**
     * If true, backspace at an indent level goes back a full indent level.
     * If false, backspace always goes back one column.
     */
    private TCheckBox backspaceUnindents;

    /**
     * If true, highlight Java keywords in text files.
     */
    private TCheckBox highlightKeywords;

    /**
     * If true, save files with tab characters.  If false, convert tabs to
     * spaces when saving files.
     */
    private TCheckBox saveWithTabs;

    /**
     * If true, trim trailing whitespace from lines and trailing empty lines
     * from the file automatically on save.
     */
    private TCheckBox trimWhitespace;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param parent the main application
     */
    public EditorOptionsWindow(final TranquilApplication parent) {

        super(parent, i18n.getString("windowTitle"), 0, 0, 66, 15,
            MODAL | CENTERED);

        TranquilApplication app = ((TranquilApplication) getApplication());

        final int buttonOffset = 14;

        // Create a status bar
        statusBar = newStatusBar(i18n.getString("statusBar"));

        // Editor options
        addLabel(i18n.getString("undoLevel"), 3, 3, "ttext", false,
            new TAction() {
                public void DO() {
                    undoLevel.activate();
                }
            });
        undoLevel = addField(22, 3, 25, false,
            app.getOption("editor.internal.undoLevel"));

        addLabel(i18n.getString("indentLevel"), 3, 4, "ttext", false,
            new TAction() {
                public void DO() {
                    indentLevel.activate();
                }
            });
        indentLevel = addField(22, 4, 25, false,
            app.getOption("editor.internal.indentLevel"));

        backspaceUnindents = addCheckBox(3, 6,
            i18n.getString("backspaceUnindents"),
            (app.getOption("editor.internal.backspaceUnindents",
                "true").equals("true") ? true : false));

        highlightKeywords = addCheckBox(3, 7,
            i18n.getString("highlightKeywords"),
            (app.getOption("editor.internal.highlightKeywords",
                "true").equals("true") ? true : false));

        saveWithTabs = addCheckBox(3, 8,
            i18n.getString("saveWithTabs"),
            (app.getOption("editor.internal.saveWithTabs",
                "true").equals("true") ? true : false));

        trimWhitespace = addCheckBox(3, 9,
            i18n.getString("trimWhitespace"),
            (app.getOption("editor.internal.trimWhitespace",
                "true").equals("true") ? true : false));

        // Buttons
        addButton(i18n.getString("saveButton"), getWidth() - buttonOffset, 4,
            new TAction() {
                public void DO() {
                    // Copy values from window to properties, save and close
                    // window.
                    copyOptions();
                    saveOptions();
                    EditorOptionsWindow.this.close();
                }
            });

        addButton(i18n.getString("okButton"), getWidth() - buttonOffset, 6,
            new TAction() {
                public void DO() {
                    // Copy values from window to properties, close window.
                    copyOptions();
                    EditorOptionsWindow.this.close();
                }
            });

        TButton cancelButton = addButton(i18n.getString("cancelButton"),
            getWidth() - buttonOffset, 8,
            new TAction() {
                public void DO() {
                    // Don't copy anything, just close the window.
                    EditorOptionsWindow.this.close();
                }
            });

        // Save this for last: make the cancel button default action.
        activate(cancelButton);
    }

    // ------------------------------------------------------------------------
    // Event handlers ---------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // TWindow ----------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Draw the options panel.
     */
    @Override
    public void draw() {
        // Draw window and border.
        super.draw();

        CellAttributes boxColor = getTheme().getColor("ttext");

        // Editor options
        drawBox(2, 2, 50, 13, boxColor, boxColor);
        putStringXY(4, 2, i18n.getString("editorTitle"), boxColor);

    }

    // ------------------------------------------------------------------------
    // EditorOptionsWindow ----------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Copy options from window fields to the application properties.
     */
    private void copyOptions() {
        TranquilApplication app = ((TranquilApplication) getApplication());
        app.setOption("editor.internal.undoLevel",
            undoLevel.getText());
        app.setOption("editor.internal.indentLevel",
            indentLevel.getText());
        app.setOption("editor.internal.backspaceUnindents",
            backspaceUnindents.isChecked() ? "true" : "false");
        app.setOption("editor.internal.highlightKeywords",
            highlightKeywords.isChecked() ? "true" : "false");
        app.setOption("editor.internal.saveWithTabs",
            saveWithTabs.isChecked() ? "true" : "false");
        app.setOption("editor.internal.trimWhitespace",
            trimWhitespace.isChecked() ? "true" : "false");
    }

    /**
     * Save application properties.
     */
    private void saveOptions() {
        TranquilApplication app = ((TranquilApplication) getApplication());
        app.saveOptions();
     }

}
