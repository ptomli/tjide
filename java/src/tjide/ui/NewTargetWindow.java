/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package tjide.ui;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import gjexer.TAction;
import gjexer.TComboBox;
import gjexer.TExceptionDialog;
import gjexer.TField;
import gjexer.TWindow;

/**
 * NewTargetWindow is used to get a new target name to add to a project.
 */
public class NewTargetWindow extends TWindow {

    /**
     * Translated strings.
     */
    private static ResourceBundle i18n = ResourceBundle.getBundle(NewTargetWindow.class.getName());

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Type of target to prompt for.
     */
    public enum TargetType {
        /**
         * Java source file.
         */
        JAVA_SOURCE,

        /**
         * Generic text file.
         */
        TEXT,

        /**
         * JAR file.
         */
        JAR,
    }

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The name of the new target.
     */
    private TField targetName;

    /**
     * The type of the new target.
     */
    private TComboBox targetType;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.  The input box will be centered on screen.
     *
     * @param projectWindow the ProjectWindow to add the target to
     */
    public NewTargetWindow(final ProjectWindow projectWindow) {

        super(projectWindow.getApplication(), i18n.getString("windowTitle"),
            1, 1, 60, 15, CENTERED | MODAL);

        addLabel(i18n.getString("nameLabel"), 1, 2, "twindow.background.modal");
        addLabel(i18n.getString("typeLabel"), 1, 4, "twindow.background.modal");

        targetName = addField(10, 2, 30, false, "");

        List<String> targetTypes = new ArrayList<String>();
        targetTypes.add(i18n.getString(TargetType.JAVA_SOURCE.toString()));
        targetTypes.add(i18n.getString(TargetType.TEXT.toString()));
        targetTypes.add(i18n.getString(TargetType.JAR.toString()));
        targetType = addComboBox(10, 4, 30, targetTypes, 0, 5, null);

        addButton(i18n.getString("browseButton"), 42, 2,
            new TAction() {
                public void DO() {
                    try {
                        String filename = fileOpenBox(".",
                            gjexer.TFileOpenBox.Type.SELECT);

                        if (filename != null) {
                            File targetFile = new File(filename);
                            File base = new File(projectWindow.getProject().
                                getSourceDir()).getAbsoluteFile();
                            String relPath = null;
                            try {
                                relPath = ProjectOptionsWindow.getRelativePath(base, targetFile);
                            } catch (IOException e) {
                                new TExceptionDialog(getApplication(), e);
                                return;
                            }

                            // System.err.println("relPath: " + relPath);

                            if (relPath != null) {
                                if (relPath.equals("")) {
                                    targetName.setText(projectWindow.
                                        getProject().getSourceDir());
                                } else {
                                    targetName.setText(relPath);
                                }
                            } else {
                                targetName.setText(filename.trim());
                            }
                        }
                    } catch (IOException e) {
                        // Show this exception to the user.
                        new TExceptionDialog(getApplication(), e);
                    }
                }
            });

        addButton(i18n.getString("okButton"), 19, getHeight() - 4,
            new TAction() {
                public void DO() {
                    if (getTargetName().length() > 0) {
                        if (getTargetName().equals(projectWindow.getProject().
                                getSourceDir())
                        ) {
                            // Special case: we are adding everything in the
                            // project's source directory.
                            projectWindow.addTarget(".", getTargetType());
                        } else {
                            projectWindow.addTarget(getTargetName(),
                                getTargetType());
                        }
                    }
                    getApplication().closeWindow(NewTargetWindow.this);
                }
            });

        addButton(i18n.getString("cancelButton"), 31, getHeight() - 4,
            new TAction() {
                public void DO() {
                    getApplication().closeWindow(NewTargetWindow.this);
                }
            });

        activate(targetName);

    }

    // ------------------------------------------------------------------------
    // Event Handlers ---------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // TWindow ----------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // NewTargetInputBox ------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Retrieve the target name.
     *
     * @return the target name
     */
    public String getTargetName() {
        return targetName.getText().trim();
    }

    /**
     * Retrieve the target type.
     *
     * @return the target type
     */
    public TargetType getTargetType() {
        String type = targetType.getText();
        if (type.equals(i18n.getString(TargetType.JAVA_SOURCE.toString()))) {
            return TargetType.JAVA_SOURCE;
        } else if (type.equals(i18n.getString(TargetType.TEXT.toString()))) {
            return TargetType.TEXT;
        } else if (type.equals(i18n.getString(TargetType.JAR.toString()))) {
            return TargetType.JAR;
        } else {
            // Catch-all for any other type.
            return TargetType.JAVA_SOURCE;
        }
    }

}
