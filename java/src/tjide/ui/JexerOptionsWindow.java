/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package tjide.ui;

import java.util.ArrayList;
import java.util.ResourceBundle;

import gjexer.TAction;
import gjexer.TApplication;
import gjexer.TButton;
import gjexer.TCheckBox;
import gjexer.TComboBox;
import gjexer.TWindow;
import gjexer.bits.CellAttributes;

/**
 * This window is used to configure the (G)Jexer preferences.
 */
public class JexerOptionsWindow extends TWindow {

    /**
     * Translated strings.
     */
    private static ResourceBundle i18n = ResourceBundle.getBundle(JexerOptionsWindow.class.getName());

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Triple-buffer support.
     */
    private TCheckBox tripleBuffer;

    /**
     * Cursor style.
     */
    private TComboBox cursorStyle;

    /**
     * Sixel support.
     */
    private TCheckBox sixel;

    /**
     * 24-bit RGB color for normal system colors.
     */
    private TCheckBox rgbColor;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param parent the main application
     */
    public JexerOptionsWindow(final TApplication parent) {

        super(parent, i18n.getString("windowTitle"), 0, 0, 76, 24,
            MODAL | CENTERED);

        TranquilApplication app = ((TranquilApplication) getApplication());

        final int buttonOffset = 14;

        // Create a status bar
        statusBar = newStatusBar(i18n.getString("statusBar"));

        tripleBuffer = addCheckBox(3, 3, i18n.getString("tripleBuffer"),
            app.getOption("gjexer.Swing.tripleBuffer", "true").equals("true"));

        addLabel(i18n.getString("cursorStyle"), 3, 4, "ttext", false);
        ArrayList<String> cursorStyles = new ArrayList<String>();
        cursorStyles.add(i18n.getString("cursorStyleBlock").toLowerCase());
        cursorStyles.add(i18n.getString("cursorStyleOutline").toLowerCase());
        cursorStyles.add(i18n.getString("cursorStyleUnderline").toLowerCase());
        cursorStyle = addComboBox(22, 4, 25, cursorStyles, 0, 4, null);
        cursorStyle.setText(app.getOption("gjexer.Swing.cursorStyle",
                "underline").toLowerCase());

        sixel = addCheckBox(3, 12, i18n.getString("sixel"),
            app.getOption("gjexer.ECMA48.sixel", "true").equals("true"));
        rgbColor = addCheckBox(3, 13, i18n.getString("rgbColor"),
            app.getOption("gjexer.ECMA48.rgbColor", "false").equals("true"));

        // Buttons
        addButton(i18n.getString("saveButton"), getWidth() - buttonOffset, 4,
            new TAction() {
                public void DO() {
                    // Copy values from window to properties, save and close
                    // window.
                    copyOptions();
                    saveOptions();
                    JexerOptionsWindow.this.close();
                }
            });

        addButton(i18n.getString("okButton"), getWidth() - buttonOffset, 6,
            new TAction() {
                public void DO() {
                    // Copy values from window to properties, close window.
                    copyOptions();
                    JexerOptionsWindow.this.close();
                }
            });

        TButton cancelButton = addButton(i18n.getString("cancelButton"),
            getWidth() - buttonOffset, 8,
            new TAction() {
                public void DO() {
                    // Don't copy anything, just close the window.
                    JexerOptionsWindow.this.close();
                }
            });

        // Save this for last: make the cancel button default action.
        activate(cancelButton);
    }

    // ------------------------------------------------------------------------
    // Event handlers ---------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // TWindow ----------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Draw the options panel.
     */
    @Override
    public void draw() {
        // Draw window and border.
        super.draw();

        CellAttributes boxColor = getTheme().getColor("ttext");

        // Swing backend options
        drawBox(2, 2, 50, 10, boxColor, boxColor);
        putStringXY(4, 2, i18n.getString("swingTitle"), boxColor);

        // ECMA48 backend options
        drawBox(2, 11, 50, 17, boxColor, boxColor);
        putStringXY(4, 11, i18n.getString("ecma48Title"), boxColor);

    }

    // ------------------------------------------------------------------------
    // JexerOptionsWindow -----------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Copy options from window fields to the application properties.
     */
    private void copyOptions() {
        TranquilApplication app = ((TranquilApplication) getApplication());

        if (tripleBuffer.isChecked()) {
            app.setOption("gjexer.Swing.tripleBuffer", "true");
        } else {
            app.setOption("gjexer.Swing.tripleBuffer", "false");
        }

        app.setOption("gjexer.Swing.cursorStyle",
            cursorStyle.getText().toLowerCase());

        if (rgbColor.isChecked()) {
            app.setOption("gjexer.ECMA48.rgbColor", "true");
        } else {
            app.setOption("gjexer.ECMA48.rgbColor", "false");
        }

        if (sixel.isChecked()) {
            app.setOption("gjexer.ECMA48.sixel", "true");
        } else {
            app.setOption("gjexer.ECMA48.sixel", "false");
        }

        // Make these options effective for the running session.
        app.resolveOptions();
    }

    /**
     * Save application properties.
     */
    private void saveOptions() {
        TranquilApplication app = ((TranquilApplication) getApplication());
        app.saveOptions();
     }

}
