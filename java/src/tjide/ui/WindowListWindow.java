/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package tjide.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

import gjexer.TAction;
import gjexer.TApplication;
import gjexer.TButton;
import gjexer.TList;
import gjexer.TWindow;
import gjexer.bits.GraphicsChars;

/**
 * This window lists the open windows in the application.
 */
public class WindowListWindow extends TWindow {

    /**
     * Translated strings.
     */
    private static ResourceBundle i18n = ResourceBundle.getBundle(WindowListWindow.class.getName());

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The list of windows.
     */
    private TList windows;

    /**
     * The window titles, created in populateWindowList().
     */
    private HashMap<String, TWindow> windowTitles;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param parent the main application
     */
    public WindowListWindow(final TApplication parent) {

        super(parent, i18n.getString("windowTitle"),
            0, 0, 64, 20, MODAL | CENTERED);

        final int buttonOffset = 19;

        // Create a status bar
        statusBar = newStatusBar(i18n.getString("statusBar"));

        windows = addList(new ArrayList<String>(), 1, 2, 40, getHeight() - 4);

        populateWindowList();

        addLabel(i18n.getString("windowListTitle"), 1, 1,
            new TAction() {
                public void DO() {
                    WindowListWindow.this.activate(windows);
                }
            });

        addButton(i18n.getString("switchToWindowButton"),
            getWidth() - buttonOffset, 3,

            new TAction() {
                public void DO() {
                    String windowTitle = windows.getSelected();
                    TWindow switchToWindow = windowTitles.get(windowTitle);
                    if (switchToWindow != null) {
                        // Close this window, then switch to the selected
                        // window.
                        WindowListWindow.this.close();
                        switchToWindow.activate();
                    }
                }
            });

        addButton(i18n.getString("closeWindowButton"),
            getWidth() - buttonOffset, 5,

            new TAction() {
                public void DO() {
                    TranquilApplication app;
                    app = ((TranquilApplication) getApplication());

                    String windowTitle = windows.getSelected();
                    TWindow closeWindow = windowTitles.get(windowTitle);
                    if (closeWindow != null) {
                        if ((closeWindow == app.getCompileStatusWindow())
                            || (closeWindow == app.getMessageWindow())
                        ) {
                            // Don't close these permanent hidden.
                            return;
                        }

                        // Normal window, close it.
                        closeWindow.close();
                        populateWindowList();
                    }
                }
            });

        TButton cancelButton = addButton(i18n.getString("cancelButton"),
            getWidth() - buttonOffset, 10,

            new TAction() {
                public void DO() {
                    // Don't do anything, just close the window.
                    WindowListWindow.this.close();
                }
            });

        // Save this for last: make the cancel button default action.
        activate(cancelButton);
    }

    // ------------------------------------------------------------------------
    // Event handlers ---------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // TWindow ----------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // WindowListWindow -------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Populate the list of windows.
     */
    private void populateWindowList() {
        TranquilApplication app = ((TranquilApplication) getApplication());

        // Make the list of windows
        windowTitles = new HashMap<String, TWindow>();
        ArrayList<TWindow> windowList = new ArrayList<TWindow>();
        ArrayList<TWindow> hiddenWindowList = new ArrayList<TWindow>();

        for (TWindow window: getApplication().getAllWindows()) {
            if (window instanceof WindowListWindow) {
                continue;
            }
            if (window.isHidden()
                || (window == app.getCompileStatusWindow())
                || (window == app.getMessageWindow())
            ) {
                hiddenWindowList.add(window);
            } else {
                windowList.add(window);
            }
        }
        windowList.addAll(hiddenWindowList);

        ArrayList<String> windowListValues = new ArrayList<String>();
        int i = 1;
        for (TWindow window: windowList) {
            String title = String.format("%3d %c %s", i,
                GraphicsChars.VERTICAL_BAR, window.getTitle());
            windowTitles.put(title, window);
            windowListValues.add(title);
            i++;
        }
        windows.setList(windowListValues);
        if (windowListValues.size() > 0) {
            windows.setSelectedIndex(0);
        }
    }

}
