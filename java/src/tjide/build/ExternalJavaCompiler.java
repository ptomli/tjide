/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package tjide.build;

import java.io.File;
import java.io.InputStream;
import java.io.IOException;
import java.util.List;
import java.text.MessageFormat;

import tjide.build.CompileListener;
import tjide.build.CompileTask;
import tjide.project.JavaTarget;
import tjide.project.Project;
import tjide.project.Target;

/**
 * ExternalJavaCompiler compiles a single Java language source file.
 */
public class ExternalJavaCompiler implements CompileTask {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The Java target this is compiling.
     */
    private JavaTarget target;

    /**
     * The listeners to be notified at compile completion.
     */
    private List<CompileListener> listeners;

    /**
     * The external compile thread.
     */
    private ExternalJdkCompileThread externalJdk;

    /**
     * ExternalJdkCompileThread wraps a javac Process.
     */
    private class ExternalJdkCompileThread implements Runnable {

        /**
         * The process running the Jdk.
         */
        private Process jdk;

        /**
         * If true, the jdk is still running.
         */
        private volatile boolean running = true;

        /**
         * If true, destroy the jdk process.
         */
        private volatile boolean cancel = false;

        /**
         * The time at which run() was entered.
         */
        private long startTime;

        /**
         * The listeners to be notified at compile completion.
         */
        private List<CompileListener> listeners;

        /**
         * Constructor.
         *
         * @param command the command line arguments that will launch the
         * javac correctly.
         * @param listeners the listeners to notify at compile completion
         */
        public ExternalJdkCompileThread(final String [] command,
            final List<CompileListener> listeners) throws IOException {

            jdk = Runtime.getRuntime().exec(command);
            this.listeners = listeners;
        }

        /**
         * Get the running flag.
         *
         * @return true of the jdk is still running
         */
        public boolean isRunning() {
            return running;
        }

        /**
         * Set the cancel flag.
         */
        public void cancelCompile() {
            cancel = true;
        }

        /**
         * Get the time at which run() was entered.
         *
         * @return the time run() was entered in millis
         */
        public long getStartTime() {
            return startTime;
        }

        /**
         * Run the compile task.
         */
        public void run() {
            boolean ok = false;
            startTime = System.currentTimeMillis();

            try {

                InputStream stdout = jdk.getInputStream();
                InputStream stderr = jdk.getErrorStream();

                while (!cancel) {

                    // Consume whatever is in jdk's stdout and stderr.
                    int rc = stdout.available();
                    if (rc > 0) {
                        byte [] junk = new byte[rc];
                        stdout.read(junk);
                    }
                    rc = stderr.available();
                    if (rc > 0) {
                        byte [] junk = new byte[rc];
                        stderr.read(junk);
                    }

                    // See if jdk has exited.
                    try {
                        rc = jdk.exitValue();
                        if (rc == 0) {
                            ok = true;
                        }
                        break;
                    } catch (IllegalThreadStateException e) {
                        // jdk is still running, wait a bit and then check
                        // everything again.
                        Thread.sleep(10);
                    }

                    // We were asked to cancel, bail out.
                    if (cancel == true) {
                        jdk.destroy();
                    }
                } // while (!cancel)

            } catch (Throwable t) {
                t.printStackTrace();
            }

            running = false;

            for (CompileListener listener: listeners) {
                if (ok) {
                    // System.err.println("setCompileSucceeded " + listener);
                    listener.setCompileSucceeded(true);
                } else {
                    // System.err.println("setCompileFailed " + listener);
                    listener.setCompileFailed(true);
                }
            }
        }

    }

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param target the Java target this is compiling
     * @param listeners the listeners being notified of the compile
     */
    public ExternalJavaCompiler(final JavaTarget target,
        final List<CompileListener> listeners) {

        this.target = target;
        this.listeners = listeners;
    }

    // ------------------------------------------------------------------------
    // CompileTask ------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Cancel an in-progress compile.
     */
    public void cancelCompile() {
        if (externalJdk != null) {
            externalJdk.cancelCompile();
        }
    }

    /**
     * Determine if compile is in progress.
     *
     * @return true if the compile is in progress
     */
    public boolean isCompileRunning() {
        if (externalJdk != null) {
            return externalJdk.isRunning();
        }
        return false;
    }

    /**
     * Get the start time of the compile task.
     *
     * @return the system time (millis) the compile task started
     */
    public long getCompileStartTime() {
        if (externalJdk != null) {
            return externalJdk.getStartTime();
        }
        return -1;
    }

    /**
     * Get the target of the compile task.
     *
     * @return the target of the compile task
     */
    public Target getTarget() {
        return target;
    }

    // ------------------------------------------------------------------------
    // ExternalJavaCompiler ---------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Compile the target.
     *
     * @param project the project metadata
     */
    public void compile(final Project project) {
        // System.err.println("compile() " + name);

        String externalCompiler = project.getOption("compiler.java.jdkBin");
        if (externalCompiler == null) {
            return;
        }

        for (CompileListener listener: listeners) {
            listener.setCompileBegin();
            listener.setCompileTask(this);
            listener.setCompileFile(target.getName(),
                target.getBuildFilename());
            listener.setCompileLineCount(target.getSourceLineCount(project));
            listener.setCompileAvailableMemory(Runtime.getRuntime().
                freeMemory() / 1024);
        }

        String commandLine = MessageFormat.format(externalCompiler,
            project.getRootDir(), project.getSourceDir(), project.getBuildDir(),
            new File(project.getSourceDir(), target.getName()).toString());
        String [] cmd = commandLine.split("\\s+");
        // System.err.println("commandLine: '" + commandLine + "'");

        try {
            externalJdk = new ExternalJdkCompileThread(cmd, listeners);
            (new Thread(externalJdk)).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
