/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package tjide.build;

import tjide.project.Project;
import tjide.project.Target;

/**
 * CompileTask is a callback interface to externally monitor the progress of
 * a compile.
 */
public interface CompileTask {

    /**
     * Cancel an in-progress compile.  Does nothing if the compile is not
     * actually running.
     */
    public void cancelCompile();

    /**
     * Determine if compile is in progress.
     *
     * @return true if the compile is in progress
     */
    public boolean isCompileRunning();

    /**
     * Get the start time of the compile task.
     *
     * @return the system time (millis) the compile task started
     */
    public long getCompileStartTime();

    /**
     * Get the target of the compile task.
     *
     * @return the target of the compile task
     */
    public Target getTarget();

    /**
     * Compile the target.
     *
     * @param project the project metadata
     */
    public void compile(final Project project);

}
