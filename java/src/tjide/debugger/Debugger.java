/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package tjide.debugger;

import java.util.List;

import tjide.project.FileTarget;

/**
 * Debugger is the interface between the UI and a process being debugged.
 */
public interface Debugger {

    /**
     * Continue execution.
     */
    public void resume();

    /**
     * Terminate the program.
     */
    public void reset();

    /**
     * Continue one more step, including going into a new stack frame.
     */
    public void traceInto();

    /**
     * Continue one more step, skipping over a new stack frame.
     */
    public void stepOver();

    /**
     * Go to a specific file location.
     *
     * @param target the target
     * @param line the line number
     */
    public void runToLocation(final FileTarget target, final int line);

    /**
     * Add a breakpoint.
     *
     * @param target the target
     * @param line the line number
     */
    public void addBreakpoint(final FileTarget target, final int line);

    /**
     * Remove a breakpoint.
     *
     * @param target the target
     * @param line the line number
     */
    public void removeBreakpoint(final FileTarget target, final int line);

}
