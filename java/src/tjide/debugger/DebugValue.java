/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package tjide.debugger;

import java.util.List;

/**
 * DebugValue represents a modifiable variable value in the debugged process.
 */
public interface DebugValue {

    /**
     * Get the name for this value.
     *
     * @return the name
     */
    public String getName();

    /**
     * Get the type name for this value.
     *
     * @return the type name
     */
    public String getType();

    /**
     * Get a human-readable value for this value.
     *
     * @return the value
     */
    public String getValue();

    /**
     * Set a new value for this value.
     *
     * @param value the new value
     */
    public void setValue(final String value);

    /**
     * Check if this value can be modified.
     *
     * @return true if this value can be modified
     */
    public boolean canModify();

    /**
     * Check if this a compound value (array or class).
     *
     * @return true if this is a compound value such as array or class
     */
    public boolean isCompound();

    /**
     * Get the sub-values of this value, if this is a compound type.
     *
     * @return the values, or null if this is not a compound type
     */
    public List<DebugValue> getSubValues();

}
