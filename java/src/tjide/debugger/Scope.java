/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package tjide.debugger;

import java.util.List;

/**
 * Scope represents a lexical or dynamic scope for a debugged process at a
 * stopped execution location such as a breakpoint or step.
 */
public interface Scope {

    /**
     * Get the source filename.
     *
     * @return the source filename
     */
    public String getSourceFilename();

    /**
     * Get the source line number.
     *
     * @return the line number
     */
    public int getLine();

    /**
     * Get a value.
     *
     * @param expression a name that is visible within this scope.  It could
     * be local variable name or a field name.
     */
    public String getValue(final String expression);

    /**
     * Get the parent scope of this scope.
     *
     * @return the scope of the calling function, or null if already at the
     * top-level scope
     */
    public Scope getParentScope();

    /**
     * Get a human-readable description of this location on the callstack.
     *
     * @return a callstack location string
     */
    public String getCallstackString();

    /**
     * Get a human-readable description of every location on the callstack.
     *
     * @return a list of callstack location strings
     */
    public List<String> getAllCallstackStrings();

    /**
     * Get a human-readable description of every location on the callstack
     * for a particular thread.
     *
     * @param thread the thread
     * @return a list of callstack location strings
     */
    public List<String> getAllCallstackStrings(final DebugThread thread);

    /**
     * Get a list of all threads in the debugged process.
     *
     * @return list of threads
     */
    public List<DebugThread> getThreads();

    /**
     * Get the thread for this scope.
     *
     * @return the thread
     */
    public DebugThread getThread();

    /**
     * Get a list of local variables in this scope.
     *
     * @return the variables
     */
    public List<DebugValue> getLocalVariables();

    /**
     * Get a list of object ('this') variables in this scope.
     *
     * @return the variables
     */
    public List<DebugValue> getObjectVariables();

    /**
     * Get a list of class (static) variables in this scope.
     *
     * @return the variables
     */
    public List<DebugValue> getStaticVariables();

    /**
     * Get the class name of the object type ('this') in this scope.
     *
     * @return the name
     */
    public String getObjectClassName();

    /**
     * Get the hash code of the object type ('this') in this scope.
     *
     * @return the hash code, or 0 if there is no object in this scope
     */
    public int getObjectHashCode();

}
