/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package tjide.project;

/**
 * NotRunnableException is thrown by RunConfig.runTarget() when it is not
 * runnable.
 */
public class NotRunnableException extends Exception {

    /**
     * Serializable version.
     */
    private static final long serialVersionUID = 1;

    /**
     * Construct an instance with a message.
     *
     * @param msg exception text
     */
    public NotRunnableException(String msg) {
        super(msg);
    }
}
