/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package tjide.project;

/**
 * ProjectTarget is the root-level target containing other targets in a
 * project.
 */
public class ProjectTarget extends Target {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The project this target represents.
     */
    private Project project;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param project the project being worked on
     */
    public ProjectTarget(final Project project) {
        super(project.getName(), "");
        this.project = project;
    }

    // ------------------------------------------------------------------------
    // ProjectTarget ----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Check if target is stale.
     *
     * @param sourceDirName the project's source directory
     * @param buildDirName the project's build output directory
     * @return true if the target needs to be rebuilt
     */
    @Override
    public boolean isStale(final String sourceDirName,
        final String buildDirName) {

        // Projects are stale if any of their contained targets are stale.
        for (Target target: project.getTargets()) {
            if (target.isStale(sourceDirName, buildDirName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Compile the target.
     *
     * @param project the project metadata
     */
    @Override
    public void compile(final Project project) {
        // Do nothing.
    }

    /**
     * Make human-readable description of this target.
     *
     * @return displayable String
     */
    @Override
    public String toString() {
        return name;
    }

}
