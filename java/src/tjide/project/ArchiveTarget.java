/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package tjide.project;

import java.util.ArrayList;
import java.util.List;

/**
 * ArchiveTarget combines several other Targets into a new file.
 */
public abstract class ArchiveTarget extends Target {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The list of targets that will be included in this archive.
     */
    protected List<FileTarget> files = new ArrayList<FileTarget>();

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param name the source filename, relative to the project's source
     * directory name.  This will also be the name seen in the project
     * window.
     * @param buildFilename the name of the generated file of the target,
     * relative to the project's build output directory
     */
    public ArchiveTarget(final String name, final String buildFilename) {
        super(name, buildFilename);
    }

    // ------------------------------------------------------------------------
    // ArchiveTarget ----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Check if target is stale.
     *
     * @param sourceDir the project's source directory
     * @param buildDir the project's build output directory
     * @return true if the target needs to be rebuilt
     */
    @Override
    public final boolean isStale(final String sourceDir,
        final String buildDir) {

        long time = 0;
        for (FileTarget file: files) {
            // If any file is stale, then the archive is stale.
            if (file.isStale(sourceDir, buildDir)) {
                return true;
            }

            long fileTime = file.getLastBuildTime(buildDir);
            if (fileTime > time) {
                time = fileTime;
            }
        }

        return super.isStale(time, buildDir);
    }

    /**
     * Make human-readable description of this target.
     *
     * @return displayable String
     */
    @Override
    public String toString() {
        return "ARCHIVE: " + name;
    }

}
