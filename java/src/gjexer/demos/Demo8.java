/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package gjexer.demos;

import java.net.ServerSocket;
import java.net.Socket;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import gjexer.TApplication;
import gjexer.backend.*;
import gjexer.demos.DemoApplication;
import gjexer.net.TelnetServerSocket;


/**
 * This class shows off the use of MultiBackend and MultiScreen.
 */
public class Demo8 {

    /**
     * Translated strings.
     */
    private static final ResourceBundle i18n = ResourceBundle.getBundle(Demo8.class.getName());

    // ------------------------------------------------------------------------
    // Demo8 ------------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Main entry point.
     *
     * @param args Command line arguments
     */
    public static void main(final String [] args) {
        ServerSocket server = null;
        try {

            /*
             * In this demo we will create a headless application that anyone
             * can telnet to.
             */

            /*
             * Check the arguments for the port to listen on.
             */
            if (args.length == 0) {
                System.err.println(i18n.getString("usageString"));
                return;
            }
            int port = Integer.parseInt(args[0]);

            /*
             * We create a headless screen and use it to establish a
             * MultiBackend.
             */
            HeadlessBackend headlessBackend = new HeadlessBackend();
            MultiBackend multiBackend = new MultiBackend(headlessBackend);

            /*
             * Now we create the shared application (a standard demo) and
             * spin it up.
             */
            DemoApplication demoApp = new DemoApplication(multiBackend);
            (new Thread(demoApp)).start();
            multiBackend.setListener(demoApp);

            /*
             * Fire up the telnet server.
             */
            server = new TelnetServerSocket(port);
            while (demoApp.isRunning()) {
                Socket socket = server.accept();
                System.out.println(MessageFormat.
                    format(i18n.getString("newConnection"), socket));

                ECMA48Backend ecmaBackend = new ECMA48Backend(demoApp,
                    socket.getInputStream(),
                    socket.getOutputStream());

                /*
                 * Add this screen to the MultiBackend, and at this point we
                 * have the telnet client able to use the shared demo
                 * application.
                 */
                multiBackend.addBackend(ecmaBackend);

                /*
                 * Emit the connection information from telnet.
                 */
                Thread.sleep(500);
                System.out.println(MessageFormat.
                    format(i18n.getString("terminal"),
                    ((gjexer.net.TelnetInputStream) socket.getInputStream()).
                        getTerminalType()));
                System.out.println(MessageFormat.
                    format(i18n.getString("username"),
                    ((gjexer.net.TelnetInputStream) socket.getInputStream()).
                        getUsername()));
                System.out.println(MessageFormat.
                    format(i18n.getString("language"),
                    ((gjexer.net.TelnetInputStream) socket.getInputStream()).
                        getLanguage()));

            } // while (demoApp.isRunning())

            /*
             * When the application exits, kill all of the connections too.
             */
            multiBackend.shutdown();
            server.close();

            System.out.println(i18n.getString("exitMain"));

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (server != null) {
                try {
                    server.close();
                } catch (Exception e) {
                    // SQUASH
                }
            }
        }
    }

}
