/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package gjexer.help;

import java.util.List;

import gjexer.TWidget;

/**
 * TParagraph contains a reflowable collection of TWords, some of which are
 * clickable links.
 */
public class TParagraph extends TWidget {

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Topic text and links converted to words.
     */
    private List<TWord> words;

    /**
     * If true, add one row to height as a paragraph separator.  Note package
     * private access.
     */
    boolean separator = true;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param parent parent widget
     * @param words the pieces of the paragraph to display
     */
    public TParagraph(final THelpText parent, final List<TWord> words) {

        // Set parent and window
        super(parent, 0, 0, parent.getWidth() - 1, 1);

        this.words = words;
        for (TWord word: words) {
            word.setParent(this, false);
        }

        reflowData();
    }

    // ------------------------------------------------------------------------
    // TWidget ----------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // TParagraph -------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Reposition the words in this paragraph to reflect the new width, and
     * set the paragraph height.
     */
    public void reflowData() {
        int x = 0;
        int y = 0;
        for (TWord word: words) {
            if (x + word.getWidth() >= getWidth()) {
                x = 0;
                y++;
            }
            word.setX(x);
            word.setY(y);
            x += word.getWidth() + 1;
        }
        if (separator) {
            setHeight(y + 2);
        } else {
            setHeight(y + 1);
        }
    }

    /**
     * Try to select a previous link.
     *
     * @return true if there was a previous link in this paragraph to select
     */
    public boolean up() {
        if (words.size() == 0) {
            return false;
        }
        if (getActiveChild() == this) {
            // No selectable links
            return false;
        }
        TWord firstWord = null;
        TWord lastWord = null;
        for (TWord word: words) {
            if (word.isEnabled()) {
                if (firstWord == null) {
                    firstWord = word;
                }
                lastWord = word;
            }
        }
        if (getActiveChild() == firstWord) {
            return false;
        }
        switchWidget(false);
        return true;
    }

    /**
     * Try to select a next link.
     *
     * @return true if there was a next link in this paragraph to select
     */
    public boolean down() {
        if (words.size() == 0) {
            return false;
        }
        if (getActiveChild() == this) {
            // No selectable links
            return false;
        }
        TWord firstWord = null;
        TWord lastWord = null;
        for (TWord word: words) {
            if (word.isEnabled()) {
                if (firstWord == null) {
                    firstWord = word;
                }
                lastWord = word;
            }
        }
        if (getActiveChild() == lastWord) {
            return false;
        }
        switchWidget(true);
        return true;
    }


}
