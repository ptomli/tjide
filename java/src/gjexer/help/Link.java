/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package gjexer.help;

import java.util.HashSet;
import java.util.Set;
import java.util.ResourceBundle;

/**
 * A Link is a section of text with a reference to a Topic.
 */
public class Link {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The topic id that this link points to.
     */
    private String topic;

    /**
     * The text inside the link tag.
     */
    private String text;

    /**
     * The number of words in this link.
     */
    private int wordCount;

    /**
     * The word number (from the beginning of topic text) that corresponds to
     * the first word of this link.
     */
    private int index;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param topic the topic to point to
     * @param text the text inside the link tag
     * @param index the word count index
     */
    public Link(final String topic, final String text, final int index) {
        this.topic = topic;
        this.text = text;
        this.index = index;
        this.wordCount = text.split("\\s+").length;
    }

    // ------------------------------------------------------------------------
    // Link -------------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Get the topic.
     *
     * @return the topic
     */
    public String getTopic() {
        return topic;
    }

    /**
     * Get the link text.
     *
     * @return the text inside the link tag
     */
    public String getText() {
        return text;
    }

    /**
     * Get the word index for this link.
     *
     * @return the word number (from the beginning of topic text) that
     * corresponds to the first word of this link
     */
    public int getIndex() {
        return index;
    }

    /**
     * Get the number of words in this link.
     *
     * @return the number of words in this link
     */
    public int getWordCount() {
        return wordCount;
    }

    /**
     * Generate a human-readable string for this widget.
     *
     * @return a human-readable string
     */
    @Override
    public String toString() {
        return String.format("%s(%8x) topic %s link text %s word # %d count %d",
            getClass().getName(), hashCode(), topic, text, index, wordCount);
    }

}
