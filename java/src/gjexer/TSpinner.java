/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package gjexer;

import gjexer.bits.CellAttributes;
import gjexer.bits.GraphicsChars;
import gjexer.event.TKeypressEvent;
import gjexer.event.TMouseEvent;
import static gjexer.TKeypress.*;

/**
 * TSpinner implements a simple up/down spinner.
 */
public class TSpinner extends TWidget {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The action to perform when the user clicks on the up arrow.
     */
    private TAction upAction = null;

    /**
     * The action to perform when the user clicks on the down arrow.
     */
    private TAction downAction = null;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param parent parent widget
     * @param x column relative to parent
     * @param y row relative to parent
     * @param upAction action to call when the up arrow is clicked or pressed
     * @param downAction action to call when the down arrow is clicked or
     * pressed
     */
    public TSpinner(final TWidget parent, final int x, final int y,
        final TAction upAction, final TAction downAction) {

        // Set parent and window
        super(parent, x, y, 2, 1);

        this.upAction = upAction;
        this.downAction = downAction;
    }

    // ------------------------------------------------------------------------
    // Event handlers ---------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Returns true if the mouse is currently on the up arrow.
     *
     * @param mouse mouse event
     * @return true if the mouse is currently on the up arrow
     */
    private boolean mouseOnUpArrow(final TMouseEvent mouse) {
        if ((mouse.getY() == 0)
            && (mouse.getX() == getWidth() - 2)
        ) {
            return true;
        }
        return false;
    }

    /**
     * Returns true if the mouse is currently on the down arrow.
     *
     * @param mouse mouse event
     * @return true if the mouse is currently on the down arrow
     */
    private boolean mouseOnDownArrow(final TMouseEvent mouse) {
        if ((mouse.getY() == 0)
            && (mouse.getX() == getWidth() - 1)
        ) {
            return true;
        }
        return false;
    }

    /**
     * Handle mouse checkbox presses.
     *
     * @param mouse mouse button down event
     */
    @Override
    public void onMouseDown(final TMouseEvent mouse) {
        if ((mouseOnUpArrow(mouse)) && (mouse.isMouse1())) {
            up();
        } else if ((mouseOnDownArrow(mouse)) && (mouse.isMouse1())) {
            down();
        }
    }

    /**
     * Handle keystrokes.
     *
     * @param keypress keystroke event
     */
    @Override
    public void onKeypress(final TKeypressEvent keypress) {
        if (keypress.equals(kbUp)) {
            up();
            return;
        }
        if (keypress.equals(kbDown)) {
            down();
            return;
        }

        // Pass to parent for the things we don't care about.
        super.onKeypress(keypress);
    }

    // ------------------------------------------------------------------------
    // TWidget ----------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Draw the spinner arrows.
     */
    @Override
    public void draw() {
        CellAttributes spinnerColor;

        if (isAbsoluteActive()) {
            spinnerColor = getTheme().getColor("tspinner.active");
        } else {
            spinnerColor = getTheme().getColor("tspinner.inactive");
        }

        putCharXY(getWidth() - 2, 0, GraphicsChars.UPARROW, spinnerColor);
        putCharXY(getWidth() - 1, 0, GraphicsChars.DOWNARROW, spinnerColor);
    }

    // ------------------------------------------------------------------------
    // TSpinner ---------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Perform the "up" action.
     */
    private void up() {
        if (upAction != null) {
            upAction.DO(this);
        }
    }

    /**
     * Perform the "down" action.
     */
    private void down() {
        if (downAction != null) {
            downAction.DO(this);
        }
    }

}
