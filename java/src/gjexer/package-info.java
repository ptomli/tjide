/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */

/**
 * GJexer - GPL Java Text User Interface
 *
 * <p>
 * This library is a text-based windowing system loosely reminiscent of
 * Borland's <a href="http://en.wikipedia.org/wiki/Turbo_Vision">Turbo
 * Vision</a> library.  Jexer's goal is to enable people to get up and
 * running with minimum hassle and lots of polish.  A very quick "Hello
 * World" application can be created as simply as this:
 *
 * <pre>
 * {@code
 * import gjexer.TApplication;
 *
 * public class MyApplication extends TApplication {
 *
 *     public MyApplication() throws Exception {
 *         super(BackendType.XTERM);
 *
 *         // Create standard menus for Tool, File, and Window.
 *         addToolMenu();
 *         addFileMenu();
 *         addWindowMenu();
 *     }
 *
 *     public static void main(String [] args) throws Exception {
 *         MyApplication app = new MyApplication();
 *         app.run();
 *     }
 * }
 * }
 * </pre>
 */
package gjexer;
