/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package gjexer.layout;

import java.awt.Rectangle;
import java.util.HashMap;

import gjexer.TWidget;
import gjexer.event.TResizeEvent;

/**
 * StretchLayoutManager repositions child widgets based on their coordinates
 * when added and the current widget size.
 */
public class StretchLayoutManager implements LayoutManager {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Current width.
     */
    private int width = 0;

    /**
     * Current height.
     */
    private int height = 0;

    /**
     * Original width.
     */
    private int originalWidth = 0;

    /**
     * Original height.
     */
    private int originalHeight = 0;

    /**
     * Map of widget to original dimensions.
     */
    private HashMap<TWidget, Rectangle> children = new HashMap<TWidget, Rectangle>();

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param width the width of the parent widget
     * @param height the height of the parent widget
     */
    public StretchLayoutManager(final int width, final int height) {
        originalWidth = width;
        originalHeight = height;
        this.width = width;
        this.height = height;
    }

    // ------------------------------------------------------------------------
    // LayoutManager ----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Process the parent widget's resize event, and resize/reposition child
     * widgets.
     *
     * @param resize resize event
     */
    public void onResize(final TResizeEvent resize) {
        if (resize.getType() == TResizeEvent.Type.WIDGET) {
            width = resize.getWidth();
            height = resize.getHeight();
            layoutChildren();
        }
    }

    /**
     * Add a child widget to manage.
     *
     * @param child the widget to manage
     */
    public void add(final TWidget child) {
        Rectangle rect = new Rectangle(child.getX(), child.getY(),
            child.getWidth(), child.getHeight());
        children.put(child, rect);
        layoutChildren();
    }

    /**
     * Remove a child widget from those managed by this LayoutManager.
     *
     * @param child the widget to remove
     */
    public void remove(final TWidget child) {
        children.remove(child);
        layoutChildren();
    }

    /**
     * Reset a child widget's original/preferred size.
     *
     * @param child the widget to manage
     */
    public void resetSize(final TWidget child) {
        // For this layout, adding is the same as replacing.
        add(child);
    }

    // ------------------------------------------------------------------------
    // StretchLayoutManager ---------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Resize/reposition child widgets based on difference between current
     * dimensions and the original dimensions.
     */
    private void layoutChildren() {
        double widthRatio = (double) width / originalWidth;
        if (Math.abs(widthRatio) > Double.MAX_VALUE) {
            widthRatio = 1;
        }
        double heightRatio = (double) height / originalHeight;
        if (Math.abs(heightRatio) > Double.MAX_VALUE) {
            heightRatio = 1;
        }
        for (TWidget child: children.keySet()) {
            Rectangle rect = children.get(child);
            child.setDimensions((int) (rect.getX() * widthRatio),
                (int) (rect.getY() * heightRatio),
                (int) (rect.getWidth() * widthRatio),
                (int) (rect.getHeight() * heightRatio));
        }
    }

}
