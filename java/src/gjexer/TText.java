/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package gjexer;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import gjexer.bits.CellAttributes;
import gjexer.bits.StringUtils;
import gjexer.event.TKeypressEvent;
import gjexer.event.TMouseEvent;
import static gjexer.TKeypress.kbDown;
import static gjexer.TKeypress.kbEnd;
import static gjexer.TKeypress.kbHome;
import static gjexer.TKeypress.kbLeft;
import static gjexer.TKeypress.kbPgDn;
import static gjexer.TKeypress.kbPgUp;
import static gjexer.TKeypress.kbRight;
import static gjexer.TKeypress.kbUp;

/**
 * TText implements a simple scrollable text area. It reflows automatically on
 * resize.
 */
public class TText extends TScrollableWidget {

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Available text justifications.
     */
    public enum Justification {

        /**
         * Not justified at all, use spacing as provided by the client.
         */
        NONE,

        /**
         * Left-justified text.
         */
        LEFT,

        /**
         * Centered text.
         */
        CENTER,

        /**
         * Right-justified text.
         */
        RIGHT,

        /**
         * Fully-justified text.
         */
        FULL,
    }

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * How to justify the text.
     */
    private Justification justification = Justification.LEFT;

    /**
     * Text to display.
     */
    private String text;

    /**
     * Text converted to lines.
     */
    private List<String> lines;

    /**
     * Text color.
     */
    private String colorKey;

    /**
     * Maximum width of a single line.
     */
    private int maxLineWidth;

    /**
     * Number of lines between each paragraph.
     */
    private int lineSpacing = 1;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param parent parent widget
     * @param text text on the screen
     * @param x column relative to parent
     * @param y row relative to parent
     * @param width width of text area
     * @param height height of text area
     */
    public TText(final TWidget parent, final String text, final int x,
            final int y, final int width, final int height) {

        this(parent, text, x, y, width, height, "ttext");
    }

    /**
     * Public constructor.
     *
     * @param parent parent widget
     * @param text text on the screen
     * @param x column relative to parent
     * @param y row relative to parent
     * @param width width of text area
     * @param height height of text area
     * @param colorKey ColorTheme key color to use for foreground
     * text. Default is "ttext".
     */
    public TText(final TWidget parent, final String text, final int x,
            final int y, final int width, final int height,
            final String colorKey) {

        // Set parent and window
        super(parent, x, y, width, height);

        this.text = text;
        this.colorKey = colorKey;

        lines = new ArrayList<String>();

        vScroller = new TVScroller(this, getWidth() - 1, 0,
            Math.max(1, getHeight() - 1));
        hScroller = new THScroller(this, 0, getHeight() - 1,
            Math.max(1, getWidth() - 1));
        reflowData();
    }

    // ------------------------------------------------------------------------
    // TScrollableWidget ------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Override TWidget's width: we need to set child widget widths.
     *
     * @param width new widget width
     */
    @Override
    public void setWidth(final int width) {
        super.setWidth(width);
        if (hScroller != null) {
            hScroller.setWidth(getWidth() - 1);
        }
        if (vScroller != null) {
            vScroller.setX(getWidth() - 1);
        }
    }

    /**
     * Override TWidget's height: we need to set child widget heights.
     * time.
     *
     * @param height new widget height
     */
    @Override
    public void setHeight(final int height) {
        super.setHeight(height);
        if (hScroller != null) {
            hScroller.setY(getHeight() - 1);
        }
        if (vScroller != null) {
            vScroller.setHeight(getHeight() - 1);
        }
    }

    /**
     * Draw the text box.
     */
    @Override
    public void draw() {
        // Setup my color
        CellAttributes color = getTheme().getColor(colorKey);

        int begin = vScroller.getValue();
        int topY = 0;
        for (int i = begin; i < lines.size(); i++) {
            String line = lines.get(i);
            if (hScroller.getValue() < StringUtils.width(line)) {
                line = line.substring(hScroller.getValue());
            } else {
                line = "";
            }
            if (getWidth() > 3) {
                String formatString = "%-" + Integer.toString(getWidth() - 1) + "s";
                putStringXY(0, topY, String.format(formatString, line), color);
            }
            topY++;

            if (topY >= (getHeight() - 1)) {
                break;
            }
        }

        // Pad the rest with blank lines
        for (int i = topY; i < (getHeight() - 1); i++) {
            hLineXY(0, i, getWidth() - 1, ' ', color);
        }

    }

    /**
     * Handle mouse press events.
     *
     * @param mouse mouse button press event
     */
    @Override
    public void onMouseDown(final TMouseEvent mouse) {
        if (mouse.isMouseWheelUp()) {
            vScroller.decrement();
            return;
        }
        if (mouse.isMouseWheelDown()) {
            vScroller.increment();
            return;
        }

        // Pass to children
        super.onMouseDown(mouse);
    }

    /**
     * Handle keystrokes.
     *
     * @param keypress keystroke event
     */
    @Override
    public void onKeypress(final TKeypressEvent keypress) {
        if (keypress.equals(kbLeft)) {
            hScroller.decrement();
        } else if (keypress.equals(kbRight)) {
            hScroller.increment();
        } else if (keypress.equals(kbUp)) {
            vScroller.decrement();
        } else if (keypress.equals(kbDown)) {
            vScroller.increment();
        } else if (keypress.equals(kbPgUp)) {
            vScroller.bigDecrement();
        } else if (keypress.equals(kbPgDn)) {
            vScroller.bigIncrement();
        } else if (keypress.equals(kbHome)) {
            vScroller.toTop();
        } else if (keypress.equals(kbEnd)) {
            vScroller.toBottom();
        } else {
            // Pass other keys (tab etc.) on
            super.onKeypress(keypress);
        }
    }

    /**
     * Resize text and scrollbars for a new width/height.
     */
    @Override
    public void reflowData() {
        // Reset the lines
        lines.clear();

        // Break up text into paragraphs
        String[] paragraphs = text.split("\n\n");
        for (String p : paragraphs) {
            switch (justification) {
            case NONE:
                lines.addAll(Arrays.asList(p.split("\n")));
                break;
            case LEFT:
                lines.addAll(gjexer.bits.StringUtils.left(p,
                        getWidth() - 1));
                break;
            case CENTER:
                lines.addAll(gjexer.bits.StringUtils.center(p,
                        getWidth() - 1));
                break;
            case RIGHT:
                lines.addAll(gjexer.bits.StringUtils.right(p,
                        getWidth() - 1));
                break;
            case FULL:
                lines.addAll(gjexer.bits.StringUtils.full(p,
                        getWidth() - 1));
                break;
            }

            for (int i = 0; i < lineSpacing; i++) {
                lines.add("");
            }
        }
        computeBounds();
    }

    // ------------------------------------------------------------------------
    // TText ------------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Set the text.
     *
     * @param text new text to display
     */
    public void setText(final String text) {
        this.text = text;
        reflowData();
    }

    /**
     * Get the text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * Convenience method used by TWindowLoggerOutput.
     *
     * @param line new line to add
     */
    public void addLine(final String line) {
        if (StringUtils.width(text) == 0) {
            text = line;
        } else {
            text += "\n\n";
            text += line;
        }
        reflowData();
    }

    /**
     * Recompute the bounds for the scrollbars.
     */
    private void computeBounds() {
        maxLineWidth = 0;
        for (String line : lines) {
            if (StringUtils.width(line) > maxLineWidth) {
                maxLineWidth = StringUtils.width(line);
            }
        }

        vScroller.setTopValue(0);
        vScroller.setBottomValue((lines.size() - getHeight()) + 1);
        if (vScroller.getBottomValue() < 0) {
            vScroller.setBottomValue(0);
        }
        if (vScroller.getValue() > vScroller.getBottomValue()) {
            vScroller.setValue(vScroller.getBottomValue());
        }

        hScroller.setLeftValue(0);
        hScroller.setRightValue((maxLineWidth - getWidth()) + 1);
        if (hScroller.getRightValue() < 0) {
            hScroller.setRightValue(0);
        }
        if (hScroller.getValue() > hScroller.getRightValue()) {
            hScroller.setValue(hScroller.getRightValue());
        }
    }

    /**
     * Set justification.
     *
     * @param justification NONE, LEFT, CENTER, RIGHT, or FULL
     */
    public void setJustification(final Justification justification) {
        this.justification = justification;
        reflowData();
    }

    /**
     * Left-justify the text.
     */
    public void leftJustify() {
        justification = Justification.LEFT;
        reflowData();
    }

    /**
     * Center-justify the text.
     */
    public void centerJustify() {
        justification = Justification.CENTER;
        reflowData();
    }

    /**
     * Right-justify the text.
     */
    public void rightJustify() {
        justification = Justification.RIGHT;
        reflowData();
    }

    /**
     * Fully-justify the text.
     */
    public void fullJustify() {
        justification = Justification.FULL;
        reflowData();
    }

    /**
     * Un-justify the text.
     */
    public void unJustify() {
        justification = Justification.NONE;
        reflowData();
    }

}
