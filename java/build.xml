<!--

   Tranquil Java Integrated Development Environment

   The GNU General Public License Version 3

   Copyright (C) 2019 Kevin Lamonte

   This program is free software: you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->

<project name="tjide" basedir="." default="jar">

  <property name="version"       value="1.0β"/>
  <property name="src.dir"       value="src"/>
  <property name="resources.dir" value="resources"/>
  <property name="build.dir"     value="build"/>
  <property name="classes.dir"   value="${build.dir}/classes"/>
  <property name="jar.dir"       value="${build.dir}/jar"/>
  <property name="apidocs.dir"   value="docs/api"/>

  <target name="clean">
    <delete dir="${build.dir}"/>
    <delete dir="${apidocs.dir}"/>
  </target>

  <path id="project.classpath">
    <pathelement location="${src.dir}"/>
  </path>

  <target name="compile">
    <mkdir dir="${classes.dir}"/>
    <javac srcdir="${src.dir}" destdir="${classes.dir}"
           includeantruntime="true"
           classpathref="project.classpath"
           debug="on"
           debuglevel="lines,vars,source"
           target="1.6"
           source="1.6"
           >
      <compilerarg value="-Xlint"/>
      <compilerarg value="-Xdiags:verbose"/>
      <compilerarg value="-Xlint:deprecation" />
    </javac>
  </target>

  <target name="jar" depends="compile">
    <mkdir dir="${jar.dir}"/>
    <jar destfile="${jar.dir}/${ant.project.name}.jar"
         basedir="${classes.dir}">

      <fileset dir="${resources.dir}">
        <exclude name="**/*TTF-4.39.ttf" />
        <exclude name="**/*Italic*.ttf" />
      </fileset>

      <!-- Include properties files. -->
      <fileset dir="${src.dir}" includes="**/*.properties"/>

      <!-- Include source by default. -->
      <!-- <fileset dir="${src.dir}"/> -->

      <manifest>
        <attribute name="Main-Class" value="tjide.Main"/>
        <attribute name="Implementation-Version" value="${version}"/>
      </manifest>
    </jar>
  </target>

  <target name="gjexer" depends="compile">
    <mkdir dir="${jar.dir}"/>
    <jar destfile="${jar.dir}/gjexer.jar"
         basedir="${classes.dir}">

      <exclude name="**/tjide/**/*" />
      <exclude name="**/tjide/*" />
      <exclude name="*/tjide" />
      <exclude name="tjide" />

      <fileset dir="${resources.dir}">
        <exclude name="**/*TTF-4.39.ttf" />
        <exclude name="**/*Italic*.ttf" />
        <exclude name="**/*.jar" />
        <exclude name="**/licenses/**" />
        <exclude name="**/*.properties" />
        <exclude name="**/jars" />
      </fileset>

      <!-- Include properties files. -->
      <fileset dir="${src.dir}" includes="**/*.properties">
        <exclude name="**/tjide/**" />
      </fileset>

      <!-- Include source by default. -->
      <!-- <fileset dir="${src.dir}"/> -->

      <manifest>
        <attribute name="Main-Class" value="gjexer.demos.Demo1"/>
        <attribute name="Implementation-Version" value="${version}"/>
      </manifest>
    </jar>
  </target>

  <target name="run" depends="jar">
    <java jar="${jar.dir}/${ant.project.name}.jar" fork="true">
      <arg value="-Dgjexer.Swing=true"/>
    </java>
  </target>

  <target name="clean-build" depends="clean,jar"/>

  <target name="build" depends="jar"/>

  <target name="doc" depends="docs"/>


  <!--
      For Java 11+, add additionalparam="dash-dash-frames".  My
      workflow is back to Java 8, so leaving this comment here for
      myself when Debian stables moves to Java 11.
  -->

  <target name="docs" depends="jar">
    <javadoc
        destdir="${apidocs.dir}"
        author="true"
        version="true"
        use="true"
        classpathref="project.classpath"
        access="public"
        windowtitle="Tranquil Java Integrated Development Environment - API docs"
        additionalparam="--frames"
        >

      <!--  access="protected" -->
      <fileset dir="${src.dir}" defaultexcludes="yes">
        <include name="tjide/**/*.java"/>
        <include name="gjexer/**/*.java"/>
      </fileset>

      <doctitle>
        <![CDATA[<h1>Tranquil Java Integrated Development Environment</h1>]]>
      </doctitle>
      <bottom>
        <![CDATA[<i>Copyright &#169; 2019 Kevin Lamonte. Licensed GPL v3+.</i>]]>
      </bottom>
    </javadoc>
  </target>

</project>
