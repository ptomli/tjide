Tranquil Java Integrated Development Environment
================================================

Tranquil Java (TJ) is a small and fast integrated development
environment for the Java programming language.  Unlike most IDEs, TJ
is a Text User Interface (TUI) based program: it uses a single
fixed-width font for everything, making it extremely fast and
lightweight in comparison to typical Graphical User Interface (GUI)
based IDEs.

TJ's look-and-feel is loosely similiar to DOS-era IDEs such as [Turbo
Pascal](https://en.wikipedia.org/wiki/Turbo_Pascal) and [Turbo
C++](https://en.wikipedia.org/wiki/Turbo_C%2B%2B).

TJ has several other unique features that distinguish it from other
IDEs:

  * Fast startup time (1-3 seconds on i3 class CPU with spinning disk
    drive), low memory overhead (64-128MB), small disk footprint (less
    than 3 MB, plus the J2SE JDK), and no dependency on network
    connections.

  * The ability to run in both Xterm-like environments (Unix command
    line shells or over ssh) and as a Swing component for
    X11/Windows/Mac, with the same behavior and look-and-feel.

  * Image support, for both Swing and Xterm.  When running under
    Xterm, images are dithered to a fixed color palette and rendered
    as [sixel](https://en.wikipedia.org/wiki/Sixel) graphic sequences.

  * A text terminal window / shell with good Xterm/VT100 support,
    including mouse.  This enables easy use of external editors
    (e.g. vi, emacs, nano, etc.), and provides a fully-interactive
    shell as the in-development program's output window.  Terminal
    windows will also resize correctly if using
    [ptypipe](https://gitlab.com/klamonte/ptypipe).

  * Support for the raw Linux console by using
    [LCXterm](https://gitlab.com/klamonte/lcxterm) to convert GPM
    mouse events into X10 mouse events.

The TJIDE homepage, which includes additional information and binary
release downloads, is at: https://tjide.sourceforge.io .  The TJIDE
source code is hosted at: https://gitlab.com/klamonte/tjide .



License
-------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

See the file LICENSE for the full license text.

The TUI library that TJIDE is built out of is also available under the
MIT license at https://jexer.sourceforge.io .



Screenshots
-----------

![A hello world class](/screenshots/helloworld.png?raw=true "A hello world class")



Developer Information
---------------------

Issues and PRs related to Jexer or GJexer should be directed to the
[MIT version of Jexer](https://gitlab.com/klamonte/jexer) .

TJIDE is seeking additional contributors/maintainers.  Please see
[here](https://gitlab.com/klamonte/tjide/wikis/maintainers-wanted) if
you are interested in direct involvement.
